const express = require("express");
const app = express();
const bodyParser = require("body-parser");
const sqlDbFactory = require("knex");
const process = require("process");
const _ = require("lodash");

var path = require("path");

let sqlDb;

/*  Data to load  */
let locationsData = require("./other/locations.json");
let servicestypeData = require("./other/servicestype.json");
let singleServiceData = require("./other/singleservice.json");
let locationtimetableData = require("./other/locationtimetable.json");
let doctortimetableData = require("./other/doctortimetable.json");
let peopleData = require("./other/allpeople.json");
let whoData = require("./other/whoweare.json");
let contactData = require("./other/contactus.json");

function initSqlDB() {
    /* Locally we should launch the app with TEST=true to use SQLlite:  */

    let TEST = false;


    if (TEST) {
        sqlDb = sqlDbFactory({
            client: "sqlite3",
            debug: true,
            connection: {
                filename: "./childreamsdb.sqlite"
            },
            useNullAsDefault: true
        });
    } else {
        sqlDb = sqlDbFactory({
            debug: true,
            client: "pg",
            connection: process.env.DATABASE_URL,
            ssl: true
        });
    }
}

function initDb() {
    /*  Checks if locations table exists and, if does not, creates a new one mapping the attributes from json   */
    sqlDb.schema.hasTable("locations").then(exists => {
        if (!exists) {
            sqlDb.schema
                .createTable("locations", table => {
                    table.increments();
                    table.string("name");
                    table.text("quote");
                    table.text("description");
                    table.integer("serviceid");
                    table.string("latitude");
                    table.string("longitude");
                    table.string("address");
                    table.string("phone");
                })
                .then(() => {
                    return Promise.all(
                        _.map(locationsData, p => {
                            delete p.id;
                            return sqlDb("locations").insert(p);
                        })
                    );
                });
        } else {
            return true;
        }
    });
    /*  Checks if type-of-service table exists and, if does not, creates a new one mapping the attributes from json  */
    sqlDb.schema.hasTable("servicestype").then(exists => {
        if (!exists) {
            sqlDb.schema
                .createTable("servicestype", table => {
                    table.increments();
                    table.string("title");
                    table.string("briefdescription");
                    table.text("introduction");
                })
                .then(() => {
                    return Promise.all(
                        _.map(servicestypeData, s => {
                            delete s.id;
                            return sqlDb("servicestype").insert(s);
                        })
                    );
                });
        }
    });
    /*  Checks if single-service table exists and, if does not, creates a new one mapping the attributes from json  */
    sqlDb.schema.hasTable("singleservice").then(exists => {
        if (!exists) {
            sqlDb.schema
                .createTable("singleservice", table => {
                    table.increments();
                    table.string("name");
                    table.integer("typeid");
                    table.integer("supervisorid");
                    table.text("introduction");
                    table.integer("locationid");
                    table.string("briefdescription");
                    table.text("description");
                })
                .then(() => {
                    return Promise.all(
                        _.map(singleServiceData, s => {
                            delete s.id;
                            return sqlDb("singleservice").insert(s);
                        })
                    );
                });
        }
    });
    //checks if people table exists and, if does not, creates a new one mapping the attributes from json
    sqlDb.schema.hasTable("people").then(exists => {
        if (!exists) {
            sqlDb.schema
                .createTable("people", table => {
                    table.increments();
                    table.string("name");
                    table.text("description");
                    table.string("serviceid");
                    table.string("title");
                    table.string("email");
                    table.string("phone");
                    table.string("type");
                })
                .then(() => {
                    return Promise.all(
                        _.map(peopleData, p => {
                            delete p.id;
                            return sqlDb("people").insert(p);
                        })
                    );
                });
        }
    });
    //checks if who-we-are table exists and, if does not, creates a new one mapping the attributes from json
    sqlDb.schema.hasTable("whoweare").then(exists => {
        if (!exists) {
            sqlDb.schema
                .createTable("whoweare", table => {
                    table.increments();
                    table.string("title");
                    table.text("description");
                })
                .then(() => {
                    return Promise.all(
                        _.map(whoData, w => {
                            delete w.id;
                            return sqlDb("whoweare").insert(w);
                        })
                    );
                });
        }
    });
    //checks if location-timetable table exists and, if does not, creates a new one mapping the attributes from json
    sqlDb.schema.hasTable("locationtimetable").then(exists => {
        if (!exists) {
            sqlDb.schema
                .createTable("locationtimetable", table => {
                    table.increments();
                    table.integer("locationid");
                    table.string("day");
                    table.string("opening");
                    table.string("closure");
                })
                .then(() => {
                    return Promise.all(
                        _.map(locationtimetableData, s => {
                            delete s.id;
                            return sqlDb("locationtimetable").insert(s);
                        })
                    );
                });
        }
    });
    //checks if doctor-timetable table exists and, if does not, creates a new one mapping the attributes from json
    sqlDb.schema.hasTable("doctortimetable").then(exists => {
        if (!exists) {
            sqlDb.schema
                .createTable("doctortimetable", table => {
                    table.increments();
                    table.integer("peopleid");
                    table.string("name");
                    table.string("day");
                    table.string("time");
                    table.string("location");
                    table.integer("locationid");
                })
                .then(() => {
                    return Promise.all(
                        _.map(doctortimetableData, s => {
                            delete s.id;
                            return sqlDb("doctortimetable").insert(s);
                        })
                    );
                });
        }
    });
    //checks if contacts table exists and, if does not, creates a new one mapping the attributes from json
    sqlDb.schema.hasTable("contactus").then(exists => {
        if (!exists) {
            sqlDb.schema
                .createTable("contactus", table => {
                    table.increments();
                    table.string("title");
                    table.string("phone");
                    table.string("email");
                })
                .then(() => {
                    return Promise.all(
                        _.map(contactData, c => {
                            delete c.id;
                            return sqlDb("contactus").insert(c);
                        })
                    );
                });
        }
    });
    //checks if subscription-form table exists and, if does not, creates a new one mapping the attributes from json
    sqlDb.schema.hasTable("subscription").then(exists => {
        if (!exists) {
            sqlDb.schema
                .createTable("subscription", table => {
                    table.increments();
                    table.string("email");
                    table.string("name");
                    table.string("surname");
                    table.bigInteger("phone");
                    table.string("address");
                })
                .then(() => { });
        }
    });
    //checks if request&info-form table exists and, if does not, creates a new one mapping the attributes from json
    sqlDb.schema.hasTable("requestform").then(exists => {
        if (!exists) {
            sqlDb.schema
                .createTable("requestform", table => {
                    table.increments();
                    table.string("name");
                    table.string("surname");
                    table.string("email");
                    table.bigInteger("phone");
                    table.text("question");
                })
                .then(() => { });
        }
    });
}


app.use(express.static(path.join(__dirname, './public')));
app.use('/', express.static(path.join(__dirname, './public/pages')));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));


/**
 * 
 *      REST API Interface 
 * 
 */


/**
 * 
 * Locations
 * 
 */

//Get all locations in alphabetical order
app.get("/locations", function (req, res) {
    let myQuery = sqlDb("locations");
    myQuery = myQuery.orderBy("name", "asc");
    myQuery.then(result => {
        res.send(JSON.stringify(result));
    });
});

//Get a specific location
app.get("/locations/:id", function (req, res) {
    let idLocation = parseInt(req.params.id);
    //extract the location
    sqlDb("locations").where('id', idLocation).then(location => {
        //extract all the services offered by that location
        sqlDb('singleservice').select('singleservice.id', 'singleservice.name').innerJoin('locations', 'locations.serviceid', "singleservice.id").where('singleservice.locationid', idLocation).then(services => {
            let result = {
                location,
                "services": services
            };
            res.send(JSON.stringify(result));
        })
    });
});

//Get all the locations timetables gruoped by location name
app.get("/locationtimetable", function (req, res) {
    let myQuery = sqlDb("locationtimetable");
    myQuery = myQuery.orderBy("location", "asc");
    myQuery.then(result => {
        res.send(JSON.stringify(result));
    });
});

//Get a specific timetable of a location
app.get("/locationtimetable/:locationid", function (req, res) {
    let locationid = parseInt(req.params.locationid);
    sqlDb("locationtimetable").where('locationid', locationid).then(result => {
        res.send(JSON.stringify(result));
    })
});

/**
 * 
 *   Services  
 * 
*/

/*  Get a list of all type of services (based on the type of user)  */
app.get("/toservice", function (req, res) {
    sqlDb("servicestype").select('id','title', 'briefdescription').then(result => {
        res.send(JSON.stringify(result));
    });
});

/*  Get a specific type of services  */
app.get("/toservice/:id", function (req, res) {
    let serviceid = parseInt(req.params.id);
    let myQuery = sqlDb("servicestype").select('title','introduction').where('id', serviceid);
    myQuery.then(result => {
        res.send(JSON.stringify(result));
    });
});

/*  Get the list of all services of a certain type  */
app.get("/toservice/:id/singleservice", function (req, res) {
    let serviceid = parseInt(req.params.id);
    sqlDb("singleservice").select('name', 'briefdescription', 'id').where('typeid', serviceid).then(result => {
        console.log(JSON.stringify(result));
        res.send(JSON.stringify(result));
    });
});

/*  Get a single service  */
app.get("/singleservice/:id", function (req, res) {
    let serviceid = parseInt(req.params.id);

    sqlDb("singleservice").select('id','name','typeid','supervisorid','introduction','locationid','description').where('singleservice.id', serviceid).then(service => {
        //extract the location
        sqlDb("locations").select('locations.id', 'locations.name').where('locations.id', service[0].locationid).then(location => {
            //extract the supervisor
            sqlDb("people").select('people.id', 'people.name', 'people.type').where('people.id', service[0].supervisorid).then(supervisor => {
                //extract the type of service
                sqlDb("servicestype").select('servicestype.id', 'servicestype.title').where('servicestype.id', service[0].typeid).then(serviceType => {
                    let result = {
                        service,
                        location,
                        supervisor,
                        'type': serviceType
                    }
                    console.log(JSON.stringify(result));
                    res.send(JSON.stringify(result));
                })
            })
        })
    })
});

/*  Get all services  */
app.get("/singleservice", function (req, res) {

    sqlDb("singleservice").select('name', 'briefdescription', 'id', 'locationid').then(service => {
        //extract the location
        sqlDb("locations").select('locations.id', 'locations.name').join('singleservice', 'locations.id', 'singleservice.locationid').then(location => {
            let result = {
                service,
                location
            }
            console.log(JSON.stringify(result));
            res.send(JSON.stringify(result));
        })
    })
});

/**
 * 
 * People
 * 
 */

//All people in alphabetical order
app.get("/people", function (req, res) {
    let myQuery = sqlDb("people");
    myQuery = myQuery.orderBy("name", "asc");
    myQuery.then(result => {
        res.send(JSON.stringify(result));
    });
});

//Get a specific person
app.get("/people/:id", function (req, res) {
    let nameId = parseInt(req.params.id);
    //extract the service related to thet person
    sqlDb("people").where('people.id', nameId).then(people => {
        sqlDb("singleservice").select('singleservice.id', 'singleservice.name').where('singleservice.id', people[0].serviceid).then(service => {
            let result = {
                people,
                service
            }
            console.log(JSON.stringify(result));
            res.send(JSON.stringify(result));
        });
    })
});

//Get all doctors' timetable grouped by doctor's name
app.get("/doctortimetable", function (req, res) {
    let myQuery = sqlDb("doctortimetable");
    myQuery = myQuery.orderBy("name", "asc");
    myQuery.then(result => {
        res.send(JSON.stringify(result));
    });
});

//Get a specific timetable of a doctor
app.get("/doctortimetable/:peopleid", function (req, res) {
    let docTableId = parseInt(req.params.peopleid);
    sqlDb("doctortimetable").where("peopleid", docTableId).then(
        result => {
            res.send(JSON.stringify(result));
        });
});

//Single topics

//Who we are 
app.get("/whoweare", function (req, res) {
    let myQuery = sqlDb("whoweare");
    myQuery = myQuery.orderBy("title", "asc");
    myQuery.then(result => {
        res.send(JSON.stringify(result));
    });
});

//Contact us
app.get("/contactus", function (req, res) {
    let myQuery = sqlDb("contactus");
    myQuery = myQuery.orderBy("title", "asc");
    myQuery.then(result => {
        res.send(JSON.stringify(result));
    });
});


//Set the subscribers' informations with which they have filled the subscription form
app.post("/subscription", function (req, res) {
    let toappend = {
        email: req.body.email,
        name: req.body.name,
        surname: req.body.surname,
        phone: req.body.phone,
        address: req.body.address,
    };

    sqlDb("subscription").insert(toappend).then(() => {
        res.send("200");
    });
});


//Set the subscribers' requests with which they have filled the request&info form 
app.post("/requestform", function (req, res) {
    let toappend = {
        name: req.body.name,
        surname: req.body.surname,
        email: req.body.email,
        phone: req.body.phone,
        question: req.body.question,
    };

    sqlDb("requestform").insert(toappend).then(() => {
        res.send("200");
    });
});

/**
 * Set the server port
 */
let serverPort = process.env.PORT || 5000;

app.set("port", serverPort);

initSqlDB();
initDb();

/* Start the server on port 5000 */
app.listen(serverPort, function () {
    console.log(`Your app is ready at port ${serverPort}`);
});