function sendForm(){
	var name = document.getElementById('inputName').value;
	var surname = document.getElementById('inputSurname').value;
	var email = document.getElementById('inputEmail4').value;
	var phone = document.getElementById('inputPhone').value;
	var address = document.getElementById('inputAddress').value;
	
	if(isFormFilled(name, surname, email, phone, address)) {
		let formData = {
		name,
		surname,
		email,
		phone,
		address
	};
	
	console.log(JSON.stringify(formData));
	
	$.post('/subscription', formData,'json').then(res => {
	$("#form-subscription")[0].reset();

		$(".alert").remove();
		if (res == "200") {
				$("#form-subscription").append(`<div class="alert alert-success" role="alert">Thank you, you will be contacted soon!</div>`);
		} else {
				$("#form-subscription").append(`<div class="alert alert-danger" role="alert">Ooouch!! Error happen...</div>`);
		}
});
	}	
	
	else {
        $(".alert").remove();
        $("#form-subscription").append(`<div class="alert alert-danger" role="alert">Hey man, you have to fill all entries!</div>`);
	}
}

function isFormFilled(name, surname, email, phone, address) {
	if (!name || !surname || !email || !phone || !address) {
		return false;
	}
	return true;
}