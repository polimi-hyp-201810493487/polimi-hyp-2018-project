var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};



$(document).ready(function () {

    $.get("/singleservice/" + getUrlParameter('id'), function (data, status) {
        let serviceData = JSON.parse(data);
        console.log(JSON.parse(data));
        $("#subtitle").append(`
            ${serviceData.service[0].name}
        `);

        //Append supervisor data
        $("#supervisor").append(`
            <a href="../people/${serviceData.supervisor[0].type}.html?id=${serviceData.supervisor[0].id}" class="active">
								<img src="../../assets/img/people/${serviceData.supervisor[0].type}/${serviceData.supervisor[0].name}.jpg" class="supervisor-image img-responsive" alt="OurSupervisor" style="width:100%">
								<div class="card-container">
									<h4 style="color: #52d3aa">
										${serviceData.supervisor[0].name}
									</h4>
								</div>
                            </a>`
        );

        //Append the service introduction
        $("#description-center").append(`
            ${serviceData.service[0].introduction}
        `);

        //Append the locatin data
        $("#location").append(`
        <a  href="../location/single-location.html?id=${serviceData.location[0].id}" class="active">
            <img src="../../assets/img/location/${serviceData.location[0].name} - allLocations.jpg" class="supervisor-image img-responsive" alt="Our Location" style="width:100%">
			<div class="card-container">
				<h4 style="color: #52d3aa">
					${serviceData.location[0].name}
				</h4>
			</div>
        </a>
        `);

        //Append service description
        $("#description").append(`
            ${serviceData.service[0].description}
        `);

        //Create the image carousel
        for (let i = 1; i < 3; i++) {
            $(".gallery-carousel").append(`
            <div class="item"><img src="../../assets/img/service/${serviceData.service[0].name} carousel${i}.jpg" class="img-responsive"></div>
            `)
        }

        var galleryCarousel = function () {

            var owl = $('.gallery-carousel');
            owl.owlCarousel({
                items: 1,
                loop: true,
                margin: 0,
                nav: false,
                dots: true,
                smartSpeed: 800,
            });

        };

        //Create back button
        $("#back-button").append(`
        <a href="toservice.html?id=${serviceData.type[0].id}"><button class="button" >Back to ${serviceData.type[0].title}</button></a>
        `);


        $(function () {
            galleryCarousel();
        });
    });


});

