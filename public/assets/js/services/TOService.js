var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};

$(document).ready(function () {
    
    $.get("/toservice/" + getUrlParameter('id'), function (data, status) {
        console.log(JSON.parse(data));
        let TOService = JSON.parse(data);
        $("#subtitle").append(`
            ${TOService[0].title}
            `);
        $("#description").append(`
                ${TOService[0].introduction}
                `);
        $("#back-to").append(`
        <div class="row">
            <div class="col-md-12 col-xs-12 center">
                <a href="Services.html"><button class="button">Back to ${TOService[0].title}</button></a>
            </div>
        </div>

        `);
    });

    $.get("/toservice/" + getUrlParameter('id') + "/singleservice", function (data, status) {
        console.log(JSON.parse(data));
        let serviceList = JSON.parse(data);
        let i = 0;
        for (i = 0; i < serviceList.length; i++) {
            $("#servicesList").append(`
            <div class="col-xs-12 col-sm-4 col-md-4">
				<div class="recent-work-wrap">
                    <img class="img-responsive" src="../../assets/img/service/${serviceList[i].name}.jpg" alt="">
                    <div class="overlay">
					    <div class="recent-work-inner">
                            <h3>${serviceList[i].name}</h3>
                            <p>${serviceList[i].briefdescription}</p>
                            <a href="service.html?id=${serviceList[i].id}" class="more">More</a>
                        </div>
					</div>
				</div>
            </div>
        `)
        }
    });
});