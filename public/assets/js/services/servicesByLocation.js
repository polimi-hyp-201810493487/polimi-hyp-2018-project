$(document).ready(function () {
    $.get("/singleservice", function (data, status) {
        console.log(JSON.parse(data));
        var serviceList = JSON.parse(data);



        $('.list').append(`
        <li><a onclick="filterSelection('all1')">All locations</a></li>
                    `);

        for (var i = 0; i < serviceList.service.length; i++) {
            $('.list').append(`
            <li><a onclick="filterSelection('${serviceList.location[i].name}')">${serviceList.location[i].name}</a></li>
                    `);
        }


        for (i = 0; i < serviceList.service.length; i++) {

            let locationName;

            for (var x = 0; x < serviceList.location.length; x++) {
                if (serviceList.location[i].id == serviceList.service[x].locationid) {
                    locationName = serviceList.location[i].name;
                }
            }


            $('#servicesList').append(`
                <div id="${locationName}" class="col-xs-12 col-sm-4 col-md-4 show serviceFilter">
                <div class="recent-work-wrap">
                    <img class="img-responsive" src="../../assets/img/service/${serviceList.service[i].name}.jpg" alt="">
                    <div class="overlay">
                        <div class="recent-work-inner">
                            <h3>${serviceList.service[i].name}</h3>
                            <p>${serviceList.service[i].briefdescription}</p>
                            <a href="service.html?id=${serviceList.service[i].id}" class="more">More</a>
                        </div>
                    </div>
                </div>
            </div>
                `);
        }
    })
});

function filterSelection(c) {
    var x, i;
    x = document.getElementsByClassName("serviceFilter");
    if (c == "all1") {
        c = "";
    }
    for (i = 0; i < x.length; i++) {
        removeClass(x[i], "show");
        removeClass(x[i], "not-show");
        if (x[i].id.indexOf(c) > -1) {
            addClass(x[i], "show");
        }
        else {
            addClass(x[i], "not-show");
        }

    }
}

function addClass(element, name) {
    var i, array1, array2;
    array1 = element.className.split(" ");
    array2 = name.split(" ");
    for (i = 0; i < array2.length; i++) {
        if (array1.indexOf(array2[i]) == -1) {
            element.className += " " + array2[i];
        }
    }
}

function removeClass(element, name) {
    var i, array1, array2;
    array1 = element.className.split(" ");
    array2 = name.split(" ");
    for (i = 0; i < array2.length; i++) {
        while (array1.indexOf(array2[i]) > -1) {
            array1.splice(array1.indexOf(array2[i]), 1);
        }
    }
    element.className = array1.join(" ");
}