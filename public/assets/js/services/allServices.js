$(document).ready(function () {

    $.get("/toservice", function (data, status) {
        console.log(JSON.parse(data))
        var servicesType = JSON.parse(data)
        for (var i = 0; i < servicesType.length; i++) {
            $("#servicesTypeList").append(`
            <div class="col-xs-12 col-sm-4 col-md-4">
					<div class="recent-work-wrap">
						<img class="img-responsive" src="../../assets/img/service/${servicesType[i].title}.jpg" alt="">
						<div class="overlay">
							<div class="recent-work-inner">
								<div class="recent-work-inner">
                                    <h3>${servicesType[i].title}</h3>
                                    <p>${servicesType[i].briefdescription}</p>
                                    <a href="toservice.html?id=${servicesType[i].id}" class="more">More</a>
                                </div>
							</div>
						</div>
					</div>
				</div>
            `);
        }
    });
});