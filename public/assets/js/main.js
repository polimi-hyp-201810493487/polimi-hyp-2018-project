/*jslint browser: true*/
/*global $, setTimeout*/

    
    ////////////
    ///Navbar///
    ////////////

    $(window).scroll(function () {
        if ($(document).scrollTop() > 35) {
            $('.gtco-nav').addClass('transparent');
            $('.gtco-nav a').addClass('text-white');
            $('.gtco-nav ul li a').addClass('primary-effect');
            $('.gtco-nav ul li.has-dropdown .dropdown').addClass('invert-box');
            $('.gtco-nav ul li.has-dropdown .dropdown li a').addClass('primary-effect');
            $('.gtco-nav ul li.has-dropdown .dropdown, .gtco-nav ul li.has-dropdown ').addClass('secondary-effect');
            $('.gtco-nav ul li.has-dropdown .dropdown').addClass('white-pointer');
        } else {
            $('.gtco-nav').removeClass('transparent');
            $('.gtco-nav a').removeClass('text-white');
            $('.gtco-nav ul li a').removeClass('primary-effect');
            $('.gtco-nav ul li.has-dropdown .dropdown').removeClass('invert-box');
            $('.gtco-nav ul li.has-dropdown .dropdown li a').removeClass('primary-effect');
            $('.gtco-nav ul li.has-dropdown .dropdown, .gtco-nav ul li.has-dropdown').removeClass('secondary-effect');
            $('.gtco-nav ul li.has-dropdown .dropdown').removeClass('white-pointer');
        }
    });



(function () {

    'use strict';

    var window,
        fActive = '',
        mobileMenuOutsideClick,
        offcanvasMenu,
        clone1,
        clone2,
        clone3,
        burgerMenu,
        contentWayPoint,
        dropdown,
        tabs,
        goToTop,
        loaderPage,
        testimonialCarousel,
        galleryCarousel,
        document;
    

    function filterGroup(group) {
        if (fActive !== group) {
            $('.filter').filter('.' + group).slideDown();
            $('.filter').filter(':not(.' + group + ')').slideUp();
            fActive = group;
        }
    }

    $('.f-group-1').click(function () {
        filterGroup('group-1');
    });
    $('.f-group-2').click(function () {
        filterGroup('group-2');
    });
    $('.f-group-3').click(function () {
        filterGroup('group-3');
    });

    $('.f-all').click(function () {
        $('.filter .group-1').slideDown();
        $('.group-2').slideDown();
        $('.group-3').slideDown();
        fActive = 'all';
    });

    mobileMenuOutsideClick = function () {

        $(document).click(function (e) {
            var container = $("#gtco-offcanvas, .js-gtco-nav-toggle");
            if (!container.is(e.target) && container.has(e.target).length === 0) {
                if ($('body').hasClass('offcanvas')) {
                    $('body').removeClass('offcanvas');
                    $('.js-gtco-nav-toggle').removeClass('active');
                }
            }
        });

    };


    offcanvasMenu = function () {

        $('#page').prepend('<div id="gtco-offcanvas" />');
        $('#page').prepend('<a href="#" class="js-gtco-nav-toggle gtco-nav-toggle gtco-nav-black"><i></i></a>');
        clone1 = $('.menu-1 > ul').clone();
        $('#gtco-offcanvas').append(clone1);
        clone2 = $('.menu-2 > ul').clone();
        $('#gtco-offcanvas').append(clone2);
        clone3 = $('.aboutus > div').clone();
        $('#gtco-offcanvas').append(clone3);

        $('#gtco-offcanvas .has-dropdown').addClass('offcanvas-has-dropdown');
        $('#gtco-offcanvas')
            .find('li')
            .removeClass('has-dropdown');

        // Hover dropdown menu on mobile
        $('.offcanvas-has-dropdown').mouseenter(function () {
            var $this = $(this);

            $this
                .addClass('active')
                .find('ul')
                .slideDown(500, 'easeOutExpo');
        }).mouseleave(function () {

            var $this = $(this);
            $this
                .removeClass('active')
                .find('ul')
                .slideUp(500, 'easeOutExpo');
        });
        
        
        $(window).resize(function () {

            if ($('body').hasClass('offcanvas')) {
                $('body').removeClass('offcanvas');
                $('.js-gtco-nav-toggle').removeClass('active');
            }
        });
    };


    burgerMenu = function () {

        $('body').on('click', '.js-gtco-nav-toggle', function (event) {
            var $this = $(this);


            if ($('body').hasClass('overflow offcanvas')) {
                $('body').removeClass('overflow offcanvas');
            } else {
                $('body').addClass('overflow offcanvas');
            }
            $this.toggleClass('active');
            event.preventDefault();

        });
    };



    contentWayPoint = function () {
        var i = 0;
        $('.animate-box').waypoint(function (direction) {

            if (direction === 'down' && !$(this.element).hasClass('animated-fast')) {
                i = i + 1;
                $(this.element).addClass('item-animate');
                setTimeout(function () {

                    $('body .animate-box.item-animate').each(function (k) {
                        var el = $(this);
                        setTimeout(function () {
                            var effect = el.data('animate-effect');
                            if (effect === 'fadeIn') {
                                el.addClass('fadeIn animated-fast');
                            } else if (effect === 'fadeInLeft') {
                                el.addClass('fadeInLeft animated-fast');
                            } else if (effect === 'fadeInRight') {
                                el.addClass('fadeInRight animated-fast');
                            } else {
                                el.addClass('fadeInUp animated-fast');
                            }

                            el.removeClass('item-animate');
                        }, k * 200, 'easeInOutExpo');
                    });

                }, 100);

            }

        }, {
            offset: '85%'
        });
    };


    dropdown = function () {

        $('.has-dropdown').mouseenter(function () {

            var $this = $(this);
            $this
                .find('.dropdown')
                .css('display', 'block')
                .addClass('animated-fast fadeInUpMenu');

        }).mouseleave(function () {
            var $this = $(this);

            $this
                .find('.dropdown')
                .css('display', 'none')
                .removeClass('animated-fast fadeInUpMenu');
        });

    };




    tabs = function () {

        // Auto adjust height
        $('.gtco-tab-content-wrap').css('height', 0);
        var autoHeight = function () {

            setTimeout(function () {

                var tabContentWrap = $('.gtco-tab-content-wrap'),
                    tabHeight = $('.gtco-tab-nav').outerHeight(),
                    formActiveHeight = $('.tab-content.active').outerHeight(),
                    totalHeight = parseInt(tabHeight + formActiveHeight + 90);
                tabContentWrap.css('height', totalHeight);

                $(window).resize(function () {
                    var tabContentWrap = $('.gtco-tab-content-wrap'),
                        tabHeight = $('.gtco-tab-nav').outerHeight(),
                        formActiveHeight = $('.tab-content.active').outerHeight(),
                        totalHeight = parseInt(tabHeight + formActiveHeight + 90);
                    tabContentWrap.css('height', totalHeight);
                });

            }, 100);

        };

        autoHeight();


        // Click tab menu
        $('.gtco-tab-nav a').on('click', function (event) {

            var $this = $(this),
                tab = $this.data('tab');

            $('.tab-content')
                .addClass('animated-fast fadeOutDown');

            $('.gtco-tab-nav li').removeClass('active');

            $this
                .closest('li')
                .addClass('active');

            $this
                .closest('.gtco-tabs')
                .find('.tab-content[data-tab-content="' + tab + '"]')
                .removeClass('animated-fast fadeOutDown')
                .addClass('animated-fast active fadeIn');


            autoHeight();
            event.preventDefault();

        });
    };


    goToTop = function () {

        $('.js-gotop').on('click', function (event) {

            event.preventDefault();

            $('html, body').animate({
                scrollTop: $('html').offset().top
            }, 500, 'easeInOutExpo');

            return false;
        });

        $(window).scroll(function () {

            var $win = $(window);
            if ($win.scrollTop() > 200) {
                $('.js-top').addClass('active');
            } else {
                $('.js-top').removeClass('active');
            }

        });

    };
    
    
    //load the carousel

    $(window).load(function () {

        $('.carousels').owlCarousel({
            items: 1,
            loop: true,
            margin: 0,
            nav: false,
            dots: true,
            smartSpeed: 800,
            autoHeight: true
        });

    });
    
    testimonialCarousel = function () {
		
		var owl = $('.testim-carousel');
		owl.owlCarousel({
			items: 1,
			loop: true,
			margin: 0,
			nav: false,
			dots: true,
			smartSpeed: 800,
		});

	};
    
    
    
    // Loading page
    loaderPage = function () {
        $(".gtco-loader").fadeOut("slow");
    };

    $('a[href=#aboutus]').click(function () {
        $('html, body').animate({
            scrollTop: $($(this).attr('href')).offset().top
        }, "slow");
        return false;
    });

    $(function () {
        mobileMenuOutsideClick();
        offcanvasMenu();
        burgerMenu();
        contentWayPoint();
        dropdown();
        tabs();
        goToTop();
        testimonialCarousel();
        loaderPage();
    });
}());
