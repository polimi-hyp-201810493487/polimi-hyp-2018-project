var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
		}
};


$(document).ready(function () {
		
    $.get("/locations/" + getUrlParameter('id'), function (data, status) {
        console.log(JSON.parse(data));
        var location = JSON.parse(data);
				$("#header").append(
					`
						<h1 id="title-location">${location.location[0].name}</h1>
						<h3 id="subtitle">${location.location[0].quote}</h3>
					`);		
				$("#images").append(
					`
					<div id="carouselSlideshow" class="carousel slide slideshow-block text-center col-xs-12 col-sm-6 col-md-6 col-lg-6" data-ride="carousel">
						<ol class="carousel-indicators">
							<li data-target="#carouselSlideshow" data-slide-to="0" class="active"></li>
							<li data-target="#carouselSlideshow" data-slide-to="1"></li>
							<li data-target="#carouselSlideshow" data-slide-to="2"></li>
							<li data-target="#carouselSlideshow" data-slide-to="3"></li>
						</ol>
						<div class="carousel-inner" role="listbox">
							<div class="item active">
								<img id="img1" class="img-location img-responsive" 	src="../../assets/img/location/${location.location[0].name} 1.jpg" alt="first image">
							</div>
							<div class="item">
								<img id="img2" class="img-location img-responsive" src="../../assets/img/location/${location.location[0].name} 2.jpg" alt="second image">
							</div>
							<div class="item">
								<img id="img3" class="img-location img-responsive" src="../../assets/img/location/${location.location[0].name} 3.jpg" alt="third image">
							</div>
							<div class="item">
								<img id="img4" class="img-location img-responsive" src="../../assets/img/location/${location.location[0].name} 4.jpg" alt="third image">
							</div>
						</div>
						<a class="left carousel-control" href="#carouselSlideshow" role="button" data-slide="prev">
							<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
							<span class="sr-only">Previous</span>
						</a>
						<a class="right carousel-control" href="#carouselSlideshow" role="button" data-slide="next">
							<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
							<span class="sr-only">Next</span>
						</a>
					</div>
					`);
				
				$("#description").append(
					`
						<h4>${location.location[0].description}</h4>
					`	
				);
		
			$("#service-card").append(
				`
					<div class="card ">
						<a href="../services/service.html?id=${location.services[0].id}">
							<img class="img-card img-fluid" src="../../assets/img/location/${location.location[0].name} service.jpg" alt="Card">
								<div id="card-caption" class="container">
								<h4>${location.services[0].name}</h4>
							</div>
						</a>
					</div>
				`
					);
			
				$("#contacts").append(
				`
					<h4><b>Address:</b> ${location.location[0].address}<br><b>Phone:</b> ${location.location[0].phone}</h4>
				`
					);
     });
	
		$.get("/locationTimetable/" + getUrlParameter('id'), function (data, status) {
        console.log(JSON.parse(data));
        var locationTimetable = JSON.parse(data);
	
				$("#timetable").append(
					`
						<thead>
						<tr style="color:black">
							<th>Day</th>
							<th>Opening</th>
							<th>Closure</th>
						</tr>
						</thead>
						<tbody>
							<tr class="info">
								<td style="color:black">${locationTimetable[0].day}</td>
								<td>${locationTimetable[0].opening}</td>
								<td>${locationTimetable[0].closure}</td>
							</tr>
							<tr>
								<td style="color:black">${locationTimetable[1].day}</td>
								<td>${locationTimetable[1].opening}</td>
								<td>${locationTimetable[1].closure}</td>
							</tr>
							<tr class="info">
								<td style="color:black">${locationTimetable[2].day}</td>
								<td>${locationTimetable[2].opening}</td>
								<td>${locationTimetable[2].closure}</td>
							</tr>
							<tr>
								<td style="color:black">${locationTimetable[3].day}</td>
								<td>${locationTimetable[3].opening}</td>
								<td>${locationTimetable[3].closure}</td>
							</tr>
							<tr class="info">
								<td style="color:black">${locationTimetable[4].day}</td>
								<td>${locationTimetable[4].opening}</td>
								<td>${locationTimetable[4].closure}</td>
							</tr>
							<tr>
								<td style="color:black">${locationTimetable[5].day}</td>
								<td>${locationTimetable[5].opening}</td>
								<td>${locationTimetable[5].closure}</td>
							</tr>
							<tr class="info">
								<td style="color:black">${locationTimetable[6].day}</td>
								<td>${locationTimetable[6].opening}</td>
								<td>${locationTimetable[6].closure}</td>
							</tr>
						</tbody>
					`	
				);
			});	
});


////////////
///Navbar///
////////////

$(window).scroll(function() {
  if ($(document).scrollTop() > 35) {
    $('.gtco-nav').addClass('transparent');
		$('.gtco-nav a').addClass('text-white');
		$('.gtco-nav ul li.has-dropdown .dropdown').addClass('invert-box');
		$('.gtco-nav ul li.has-dropdown .dropdown li a').addClass('primary-effect');
		$('.gtco-nav ul li.has-dropdown .dropdown, .gtco-nav ul li.has-dropdown ').addClass('secondary-effect');
		$('.gtco-nav ul li.has-dropdown .dropdown').addClass('white-pointer');

	} else {
    $('.gtco-nav').removeClass('transparent'),
		$('.gtco-nav a').removeClass('text-white');
		$('.gtco-nav ul li.has-dropdown .dropdown').removeClass('invert-box');
		$('.gtco-nav ul li.has-dropdown .dropdown li a').removeClass('primary-effect');
		$('.gtco-nav ul li.has-dropdown .dropdown, .gtco-nav ul li.has-dropdown').removeClass('secondary-effect');
		$('.gtco-nav ul li.has-dropdown .dropdown').removeClass('white-pointer');
  }
});


///////////////
//Google Maps//
///////////////

var google;

function myMap() {
	
	$.get("/locations/" + getUrlParameter('id'), function (data, status) {
        console.log(JSON.parse(data));
        var location = JSON.parse(data);
	
	var mapCanvas = document.getElementById("map");
	var mapOptions = {
		center: new google.maps.LatLng(location.location[0].latitude, location.location[0].longitude),
		zoom: 17
	};
	var map = new google.maps.Map(mapCanvas, mapOptions);
	var marker = new google.maps.Marker({
		position: mapOptions.center,
		map: map
	});
		
	});
}

