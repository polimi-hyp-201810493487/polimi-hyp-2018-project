$(document).ready(function () {

    $.get("/locations", function (data, status) {

        console.log(JSON.parse(data))
        var locations = JSON.parse(data)
        for (var i = 0; i < locations.length ; i++) {
            $("#location-list").append(
`
	<div class="gtco-container col-xs-12 col-sm-4 col-md-4">
				<div class="card">
					<a href="single-location.html?id=${locations[i].id}">
						<img class="img-card img-fluid" src="../../assets/img/location/${locations[i].name} - allLocations.jpg" alt="Card">
						<div id="card-caption" class="container">
							<h4><b>${locations[i].name}</b></h4>
						</div>
					</a>
				</div>
			</div>
   `
            );
        }
    });
});


////////////
///Navbar///
////////////

$(window).scroll(function () {
    if ($(document).scrollTop() > 35) {
        $('.gtco-nav').addClass('transparent');
        $('.gtco-nav a').addClass('text-white');
        $('.gtco-nav ul li.has-dropdown .dropdown').addClass('invert-box');
        $('.gtco-nav ul li.has-dropdown .dropdown li a').addClass('primary-effect');
        $('.gtco-nav ul li.has-dropdown .dropdown, .gtco-nav ul li.has-dropdown ').addClass('secondary-effect');
        $('.gtco-nav ul li.has-dropdown .dropdown').addClass('white-pointer');

    } else {
        $('.gtco-nav').removeClass('transparent'),
            $('.gtco-nav a').removeClass('text-white');
        $('.gtco-nav ul li.has-dropdown .dropdown').removeClass('invert-box');
        $('.gtco-nav ul li.has-dropdown .dropdown li a').removeClass('primary-effect');
        $('.gtco-nav ul li.has-dropdown .dropdown, .gtco-nav ul li.has-dropdown').removeClass('secondary-effect');
        $('.gtco-nav ul li.has-dropdown .dropdown').removeClass('white-pointer');
    }
});


