$(document).ready(function () {
    $.get("/whoWeAre", function (data, status) {
        console.log(JSON.parse(data))
        var whoData = JSON.parse(data)
        for (var i = 0; i < whoData.length; i++) {
            if (i % 2 == 0) {
                $("#whoWeAre-list").append(`
                    <div class="who-color-row">
                        <div class="container">
                            <div class="row history-row who">
                                <div class="col-md-4 fadeshow1">
                                <img class="img-responsive rad" src="../../assets/img/singletopics/whoweare/${whoData[i].title}.png">
                                </div>
                                <div class=" col-md-8">
                                    <h3 class="who">${whoData[i].title}</h3>
                                    <p>${whoData[i].description}</p>
                                    <a href="" ><p>MORE</p></a>
                                </div>
                            </div>
                        </div>
                    </div>
                `);
            }
            else {
                $("#whoWeAre-list").append(`
                    <div class="who-color-row-1">
                        <div class="container">
                            <div class="row goal-row who">
                                <div class="col-md-8">
                                    <h3 class="who">${whoData[i].title}</h3>
                                    <p>${whoData[i].description}</p>
                                    <a href=""><p>MORE</p></a>
                                </div>
                                 <div class="col-md-4 fadeshow1">
                                <img class="img-responsive rad" src="../../assets/img/singletopics/whoweare/${whoData[i].title}.png">
                                </div>
                            </div>
                        </div>
                    </div>
                `);
            }
        }
    });
});