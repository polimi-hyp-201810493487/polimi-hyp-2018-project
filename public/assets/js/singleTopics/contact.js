$(document).ready(function () {
    $.get("/contactus", function (data, status) {
        var contactData = JSON.parse(data)
        $("#contacts").append(`
        <div class="container">
            <div class="contactsUs">
                <div class="row contacts-row">
                    <div class="col-xs-12 col-sm-4 col-md-4 contacts-top">
                        <h3>${contactData[0].title}</h3>
                        <p><b>Phone:</b> ${contactData[0].phone}</p>
                        <p><b>Email:</b> ${contactData[0].email}</p>
                    </div> 
                    <div class="col-xs-12 col-sm-4 col-md-4 contacts-top">
                        <h3>${contactData[1].title}</h3>
                        <p><b>Phone:</b> ${contactData[1].phone}</p>
                        <p><b>Email:</b> ${contactData[1].email}</p>
                    </div> 
                    <div class="col-xs-12 col-sm-4  col-md-4 contacts-top">
                        <h3>${contactData[2].title}</h3>
                        <p><b>Phone:</b> ${contactData[2].phone}</p>
                        <p><b>Email:</b> ${contactData[2].email}</p>
                    </div> 
                </div> 
            </div>
        </div>
        
        <div class="container">
            <div class="description-ask">Have a question or need more information? Provide your contact information, and we’ll reach out to you.</div>
        </div>
        <div class="container container-form">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-8 col-lg-push-2 format">
              <form action="">
                <div class="row">
                  <div class="col-25">
                    <label for="fname">First Name</label>
                  </div>
                  <div class="col-75">
                    <input type="text" id="fname" name="firstname" placeholder="Your name..">
                  </div>
                </div>
                <div class="row">
                  <div class="col-25">
                    <label for="lname">Last Name</label>
                  </div>
                  <div class="col-75">
                    <input type="text" id="lname" name="lastname" placeholder="Your last name..">
                  </div>
                </div>
                <div class="row">
                    <div class="col-25">
                        <label for="fname">Email</label>
                    </div>
                    <div class="col-75">
                        <input type="text" id="email" name="email" placeholder="Your email..">
                    </div>
                </div>
                <div class="row">
                    <div class="col-25">
                        <label for="fname">Phone</label>
                    </div>
                    <div class="col-75" style="padding-top:10px">
                        <input type="text" id="phone" name="phone" placeholder="Your phone.." style="padding-left:10px">
                    </div>
                </div>
                <div class="row">
                  <div class="col-25">
                    <label for="subject">Question</label>
                  </div>
                  <div class="col-75">
                    <textarea id="subject" name="question" placeholder="Write something.." style="height:200px"></textarea>
                  </div>
                </div>
                <div class="row">
                  <button type="button" class="btn btn-default btn-block" onclick="sendForm()">Submit</button>
                </div>
              </form>
            </div>   
        </div>
        `);
    });
});



function sendForm() {
    var name = document.getElementById('fname').value;
    var surname = document.getElementById('lname').value;
    var email = document.getElementById('email').value;
    var phone = document.getElementById('phone').value;
    var question = document.getElementById('subject').value;

    if (isFormFilled(name, surname, email, phone, question)) {
        let formData = {
            name,
            surname,
            email,
            phone,
            question
        };

        //Send the data to the server
        $.post('/requestform', formData, 'json').then(res => {

            $(".alert").remove();

            //Checks the server response
            if (res == "200") {
                $("#contacts").append(`<div class="alert alert-success" role="alert">Thank you, you will be contacted soon!</div>`);
            } else {
                $("#contacts").append(`<div class="alert alert-danger" role="alert">Ooouch!! Error happen...</div>`);
            }

        });
    }
    else {
        $(".alert").remove();
        $("#contacts").append(`<div class="alert alert-danger" role="alert">Hey man, you have to fill all entries!</div>`);
    }
}

//Check if the form has some empty fields
function isFormFilled(name, surname, email, phone, question) {
    if (!name || !surname || !email || !phone || !question) {
        return false;
    }
    return true;
}


