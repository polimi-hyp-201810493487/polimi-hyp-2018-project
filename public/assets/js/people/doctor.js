var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};

$(document).ready(function() {
    $.get("/people/" + getUrlParameter('id'),
    function (data, status) {
        var peopleData = JSON.parse(data);
        console.log(JSON.parse(data));
        //var timeTable = doctorsData[0].timeTable.split(",");
        $("#namedoc").append(`
            ${peopleData.people[0].name}
        `);
        $("#imgdoc").append(`
            <img src="../../assets/img/people/${peopleData.people[0].type}/${peopleData.people[0].name}.jpg" class="img-responsive">
        `);
        $("#descriptiondoc").append(`
            ${peopleData.people[0].description}
        `);
        $("#servicelinkdoc").append(`
            <a href="../../pages/services/service.html?id=${peopleData.service[0].id}" class="active">
                        <div class="supervisor-card center-block">
                            <img src="../../assets/img/service/${peopleData.service[0].name}.jpg" class="supervisor-image img-responsive" alt="OurSupervisor" style="width:100%">
                            <div class="card-container">
                                <h4 style="color:#52d3aa;">
                                    ${peopleData.service[0].name}
                                </h4>
                            </div>
                        </div>
                    </a>
        `);
        $("#contactsdoc").append(`
        <p><b>Phone:</b> ${peopleData.people[0].phone}</p>
        <p><b>Email:</b> ${peopleData.people[0].email}</p>

        `);
    });
    
    $.get("/doctorTimetable/" + getUrlParameter('id'), function (data, status) {
        console.log(JSON.parse(data));
        var doctorTimetable = JSON.parse(data);
        $("#tabledoc").append(`
            <table class="table table-striped">
                        <thead>
                            <tr class="head-table">
                                <th>Day</th>
                                <th>Time</th>
                                <th>Location</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>${doctorTimetable[0].day}</td>
                                <td>${doctorTimetable[0].time}</td>
                                <td><a href="../location/single-location.html?id=${doctorTimetable[0].locationid}">${doctorTimetable[0].location}</a></td>          
                            </tr>
                            <tr>
                                <td>${doctorTimetable[1].day}</td>
                                <td>${doctorTimetable[1].time}</td>
                                <td><a href="../location/single-location.html?id=${doctorTimetable[1].locationid}">${doctorTimetable[1].location}</a></td>                   
                            </tr>
                            <tr>
                                <td>${doctorTimetable[2].day}</td>
                                <td>${doctorTimetable[2].time}</td>
                                <td><a href="../location/single-location.html?id=${doctorTimetable[2].locationid}">${doctorTimetable[2].location}</a></td>                    
                            </tr>
                            <tr>
                                <td>${doctorTimetable[3].day}</td>
                                <td>${doctorTimetable[3].time}</td>
                                <td><a href="../location/single-location.html?id=${doctorTimetable[3].locationid}">${doctorTimetable[3].location}</a></td>                    
                            </tr>
                            <tr>
                                <td>${doctorTimetable[4].day}</td>
                                <td>${doctorTimetable[4].time}</td>
                                <td><a href="../location/single-location.html?id=${doctorTimetable[4].locationid}">${doctorTimetable[4].location}</a></td>                   
                            </tr>
                            <tr>
                                <td>${doctorTimetable[5].day}</td>
                                <td>${doctorTimetable[5].time}</td>
                                <td><a href="../location/single-location.html?id=${doctorTimetable[5].locationid}">${doctorTimetable[5].location}</a></td>                   
                            </tr>
                            <tr>
                                <td>${doctorTimetable[6].day}</td>
                                <td>${doctorTimetable[6].time}</td>
                                <td>${doctorTimetable[6].location}</td>                   
                            </tr>
                        </tbody>
                    </table>
        `)
    });
    
    
});