var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};

$(document).ready(function() {
    $.get("/people/" + getUrlParameter('id'),
    function (data, status) {
        var peopleData = JSON.parse(data);
        console.log(JSON.parse(data));
        $("#imgvol").append(`
            <img src="../../assets/img/people/${peopleData.people[0].type}/${peopleData.people[0].name}.jpg" class="img-responsive">
        `);
        $("#namevol").append(`
            ${peopleData.people[0].name}
        `);
        $("#lettervol").append(`
            ${peopleData.people[0].description}
            <p><br>Milan, 09/05/2018</p>
        `);
        $("#servicevol").append(`
        <a href="../../pages/services/service.html?id=${peopleData.service[0].id}" class="active">
                        <div class="supervisor-card center-block">
                            <img src="../../assets/img/service/${peopleData.service[0].name}.jpg" class="supervisor-image img-responsive" alt="OurSupervisor" style="width:100%">
                            <div class="card-container">
                                <h4 style="color:#52d3aa;">
                                    ${peopleData.service[0].name}
                                </h4>
                            </div>
                        </div>
                    </a>

`       );
        
        
    });
});