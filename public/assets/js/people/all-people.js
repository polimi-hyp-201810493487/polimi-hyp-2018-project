$(document).ready(function () {
    $.get("/people", function (data, status) {
        console.log(JSON.parse(data))
        var people = JSON.parse(data)
        for (var i = 0; i < people.length; i++) {
            if(people[i].type === "doctor"){
                    $("#doctors-list").append(`
                    <article class="col-xs-6 col-sm-4 col-md-3 col-lg-2 doc ${people[i].title} show">
                                <div class="card card-hib">
                                    <a href="doctor.html?id=${people[i].id}">
                                        <center>
                                            <img class="img-responsive" src="../../assets/img/people/${people[i].type}/trasparent/${people[i].name}.png">
                                        </center>
                                        <div class="name">${people[i].name}</div>
                                    </a>
                                </div>    
                            </article>   
                    `);
                }

            else{
                   $("#volunteers-list").append(`
                    <article class="col-xs-6 col-sm-4 col-md-3 col-lg-2 doc ${people[i].title} show">
                            <div class="card card-cin">
                                <a href="volunteer.html?id=${people[i].id}">
                                    <center>
                                        <img class="img-responsive" src="../../assets/img/people/${people[i].type}/trasparent/${people[i].name}.png">
                                    </center>
                                    <div class="name">${people[i].name}</div>
                                </a>
                            </div>    
                        </article> 
                    `);
            }
        }
    });
});

function filterSelection(c) {
    var x, i;
    x = document.getElementsByClassName("doc");
    if (c == "all1") {
        c = "";
    }
    for (i = 0; i < x.length; i++) {
        removeClass(x[i], "show");
        removeClass(x[i], "not-show");
        if (x[i].className.indexOf(c) > -1){ 
            addClass(x[i], "show");
        }
        else{
            addClass(x[i], "not-show");
        }
        
    }
}

function addClass(element, name) {
  var i, array1, array2;
  array1 = element.className.split(" ");
  array2 = name.split(" ");
  for (i = 0; i < array2.length; i++) {
    if (array1.indexOf(array2[i]) == -1) {
      element.className += " " + array2[i];
    }
  }
}

function removeClass(element, name) {
  var i, array1, array2;
  array1 = element.className.split(" ");
  array2 = name.split(" ");
  for (i = 0; i < array2.length; i++) {
    while (array1.indexOf(array2[i]) > -1) {
      array1.splice(array1.indexOf(array2[i]), 1); 
    }
  }
  element.className = array1.join(" ");
}

var btnContainer = document.getElementById("buttons");
var btns = btnContainer.getElementsByClassName("button");
for (var i = 0; i < btns.length; i++) {
  btns[i].addEventListener("click", function() {
    var current = document.getElementsByClassName("activate");
    current[0].className = current[0].className.replace(" activate", "");
    this.className += " activate";
  });
}
