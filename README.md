# General information about the team and the web application
* Heroku URL: ​ https://polimi-hyp-2018-10493487.herokuapp.com/index.html
* Bitbucket repo URL: ​ https://bitbucket.org/polimi-hyp-201810493487/polimi-hyp-2018-project/src/master/
* Team administrator: Luca Terracciano 10493487 polimi-hyp-2018-10493487
* Team member n.2: Matteo Redaelli 10526191 matteoreda996
* Team member n.3: Davide Salaorni 10526800 daveonwave

# REST API DOCUMENTATION

## Locations

** All locations **
Returns all location in alphabetical order

--> /locations [GET] <--

*Response*:
```
{
    "id": integer,
    "name": string,
    "quote": text,
    "description": text,
    "serviceId": integer,
    "latitude": string,
    "longitude": string,
    "address": string,
    "phone": string
}
```

** Single location**
Returns only the location selected by its id

--> /locations/:id [GET] <--

*Response*:
```
{
    "location": {
        "id": integer,
    	"name": string,
	    "quote": text,
    	"description": text,
	    "serviceId": integer,
    	"latitude": string,
	    "longitude": string,
    	"address": string,
	    "phone": string
	},
	"services": {
        "id": integer,
	    "name": string
	}
}
```


## Location timetable

** All location timetables **
Returns all timetable related with each location

--> /locationtimetable [GET] <--

*Response*:
```
{
    "id": integer,
    "locationid": integer,
    "day": string,
    "opening": string,
    "closure": string
}
```

** Single timetable **
Returns the timetable of a certain location by its id

--> /locationtimetable/:locationid [GET] <--

*Response*:
```
{
    "timetable": {
        "id": integer,
	    "locationid": integer,
    	"day": string,
    	"opening": string,
    	"closure": string
	},
	"location":
		{"id": integer}
}
```

## Type Of Services

** All Type Of Services **
Returns all Type of Services.

Type of Services :
- Child Education
- Family Care
- Volunteer Culture

--> /toservice [GET] <--

*Response*

```
{
    "id": integer,
    "title": string,
    "briefdescription": string
}
```
- briefdescription: string to append in the Type of Service box in Services.html

** Single Type of Service **
Returns the Type of Service with the specified Id

--> /toservice/:id [GET]<--

*Response*

```
{
    "id": integer,
    "title": string,
    "briefdescription": string,
    "introduction": text
}
```
- introduction: text to append in the single Type of Service page

## Services

** All Services **
Returns all the services

--> /singleservice [GET] <--

*Response*
```
{
    "service": {
        "name": string,
        "briefdescription": string,
        "id": integer,
        "locationid": integer
    },
    "location": {
        "id": integer,
        "name":string
    }
}
```

** All Services of a specified type **
Returns all the services of a specified type using the type id

--> /toservice/:id/singleservice [GET] <--

*Response*
```
{
    "name": string,
    "briefdescription": string,
    "id": integer,    
}
```

** Single Service **
Returns a specified service

--> /singleservice/:id [GET] <--

*Response*
```
{
    "service":{
        "id": integer,
        "name": string,
        "typeid": integer,
        "supervisorid": integer,
        "introduction": text,
        "locationid": integer,
        "description": text
    },
    "location":{
        "id":integer,
        "name": string
    },
    "supervisor":{
        "id":integer,
        "name": string,
        "type": string
    },
    "type"{
        "id":integer,
        "title": string
    }
}
```

## People

** All people **
Return all people in alphabetical order.
People are distinguished between volunteers and doctors.

- Doctor: has all the complete attributes
- Volunteer: the "phone" and "email" attributes are empty

--> /people [GET] <--

*Response*:

```
{
    "id": integer,
    "name: string,
    "description": text,
    "serviceid": integer,
    "title": string,
    "email": string,
    "phone": string,
    "type": string
}
```

- serviceid: the service performed by the person
- type: distinguishes the doctor from the volunteer

** Single person **
Returns only the person selected by its id

--> /people/:id [GET] <--

*Response*

```
{
    "people":{
        "id": integer,
        "name: string,
        "description": text,
        "serviceid": integer,
        "title": string,
        "email": string,
        "phone": string,
        "type": string
    },
    "service":{
        "id": integer,
        "name": string
    }
}
```

## Doctor timetable

** All doctor timetables **
Returns all timetable related with each doctor

--> /doctortimetable [GET] <--

*Response*:

```
{
    "id": integer,
    "peopleid": integer,
    "name": string,
    "day": string,
    "time": string,
    "location": string,
    "locationid": integer
}
```

** Single timetable **
Returns the timetable of a certain doctor by its id

--> /doctortimetable/:peopleid [GET] <--

*Response*:

```
{
    "timetable":{
        "id": integer,
        "peopleid": integer,
        "name": string,
        "day": string,
        "time": string,
        "location": string,
        "locationid": integer
    },
	"people":{
        "id": integer
    }
}
```

## Who we are

** All descriptions **
Returns the subtitles and all elements of the page

--> /whoweare [GET] <--

*Response*

```
{
    "title": string,
    "description": text 
}
```

## Contact us

** All contacts **
Returns the contacts

--> /contactus [GET] <--

*Response*

```
{
    "title": string,
    "phone": string,
    "email": string
}
```

## Contact us form

** Request informations **
Insert a new request

- name: the name of the person making the request
- surname: the surname of the person making the request
- email: the email of the person making the request
- phone: the phone of the person making the request
- question: the request of the person

--> /requestform [POST] <--

*Response*

```
{
    "id": integer,
    "toappend":{
        "name": string,
        "surname": string,
        "email": string,
        "phone": string,
        "question": text
    }
}
```

## Homepage Form

** Volunteer Subscription **
Insert credentials to join

- email: the email of the person who want to join the association
- name: the name of the person who want to join the association
- surname: the surname of the person who want to join the association
- phone: the phone of the person who want to join the association
- address: the address who want to join the association

--> /subscription [POST] <--

*Response*
```
{
    "id": integer,
    "toappend":{
        "email": string,
				"name": string,
        "surname": string,
        "phone": string,
        "address": string
    }
}
```

# IMPORTANT NOTES

For the first part delivery of the project the only working pages/links are:

- In all pages:

    - Events
    - News
    - Help Us
    - FAQ

In All People all the links are working. The five People that are supervisor of a service (relationship between service -> people) are:

- Dr. Daniel Green
- Dr. Diego Hijano
- Dr. Jeremie Estepp
- Volunteer Angelina Bouvier
- Volunteer Miranda Colombo

The other people also have a service associated but they are not supervisor of a service. We have done it in this way because in the first project part it was requested to create 5 people but when we after added the back-end part we decided to create also the other in order to have all the links working in all people page. So we thought for the evaluation it's not going to be a mistake because 5 people are linked to their specific services and also the services are linked to one supervisor. 

# DESCRIPTION

This is the repository for the Hypermedia Project A.A. 2017/2018.
We create a website for an association that wants to provide some services for the children with cognitive disabilities which are supervised by doctors and volunteers.

The site is made up of 6 sections: the homepage, the about us, the services section, the staff one, the location one and the events one(TODO in the second part of the project).

#SPLITTING THE WORK

We split the work based on these sections. 

In facts Luca Terracciano worked especially on the services section. We divided services in three sub-groups depending on the stakeholders: Child Education, Family Care and Volunteer Culture. For this reason we have created two kind of pages: the ones that are about the single service and the others that are introductory pages to the various groups. This structure is used also in the navbar menu with a dropdown.
Luca has also worked on the first version of the homepage.

Davide Salaorni has worked principally on the location section in which there is an introductory page with a list of the all locations avalaible. He has also developed all the single location pages and has contributed to complete the homepage style and structure.

Matteo Redaelli has developed the staff section with the all people page and each single people page. He has also worked on the about us section creating ContactUs and WhoWeAre pages.

# TEMPLATE USED

- http://www.free-css.com/free-css-templates/page218/oxygen

# LANGUAGES USED

To realize this project we used:

- HTML5
- CSS3
- Bootstrap
- JavaScript

# SCRIPTS USED

We also used some additional scripts:

- jQuery (https://jquery.com/)
- OwlCarousel (https://owlcarousel2.github.io/OwlCarousel2/)
- Google Api for Maps (https://developers.google.com/maps/documentation/javascript/tutorial)

In the template we found others plugin but we don't have used them.
 
# PROBLEMS

We encountered some problems during the developing phase:

- Understand the entire template in order to not "reinvent the wheel" and reuse the code without doing the classic "copy and paste".

- In the service section the carousels are made with the OwlCarousel plugin which has got a bug: the autoHeight option is not enabled because   it actually breaks the multiple carousels.

- The position of the About Us section in the footer needed to be fixed because it was going out of the screen.